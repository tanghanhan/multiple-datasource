CREATE DATABASE `multiple-datasource-demo-master`;
USE `multiple-datasource-demo-master`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `multiple-datasource-demo-master`.`t_user`(`name`) VALUES ('张三');
INSERT INTO `multiple-datasource-demo-master`.`t_user`(`name`) VALUES ('李四');
INSERT INTO `multiple-datasource-demo-master`.`t_user`(`name`) VALUES ('王五');
INSERT INTO `multiple-datasource-demo-master`.`t_user`(`name`) VALUES ('赵六');
INSERT INTO `multiple-datasource-demo-master`.`t_user`(`name`) VALUES ('孙七');
INSERT INTO `multiple-datasource-demo-master`.`t_user`(`name`) VALUES ('周八');
INSERT INTO `multiple-datasource-demo-master`.`t_user`(`name`) VALUES ('吴九');
INSERT INTO `multiple-datasource-demo-master`.`t_user`(`name`) VALUES ('郑十');

-- ----------------------------
-- Records of t_user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;

CREATE DATABASE `multiple-datasource-demo-slave`;
USE `multiple-datasource-demo-slave`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `multiple-datasource-demo-slave`.`t_user`(`name`) VALUES ('test1');
INSERT INTO `multiple-datasource-demo-slave`.`t_user`(`name`) VALUES ('test2');
INSERT INTO `multiple-datasource-demo-slave`.`t_user`(`name`) VALUES ('test3');
INSERT INTO `multiple-datasource-demo-slave`.`t_user`(`name`) VALUES ('test4');
INSERT INTO `multiple-datasource-demo-slave`.`t_user`(`name`) VALUES ('test5');
INSERT INTO `multiple-datasource-demo-slave`.`t_user`(`name`) VALUES ('test6');
INSERT INTO `multiple-datasource-demo-slave`.`t_user`(`name`) VALUES ('test7');
INSERT INTO `multiple-datasource-demo-slave`.`t_user`(`name`) VALUES ('test8');

SET FOREIGN_KEY_CHECKS = 1;
