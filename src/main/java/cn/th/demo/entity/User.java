package cn.th.demo.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 用户
 * @author tanghan
 * @date 2021/6/4 17:48
 */
@Getter
@Setter
@ToString
public class User implements Serializable {
	private static final long serialVersionUID = 5096775706837613611L;
	private Integer id;
	private String name;
}
