package cn.th.demo.dao.master;

import cn.th.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * user1 mapper
 * @author tanghan
 * @date 2021/6/4 17:54
 */
@Mapper
public interface User1Mapper {
	/**
	 * 查询列表
	 * @return 列表
	 */
	List<User> getList();
}
