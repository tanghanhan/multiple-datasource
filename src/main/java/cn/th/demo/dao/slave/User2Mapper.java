package cn.th.demo.dao.slave;

import cn.th.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * User2 mapper
 * @author tanghan
 * @date 2021/6/4 17:56
 */
@Mapper
public interface User2Mapper {
	/**
	 * 查询列表
	 * @return 列表
	 */
	List<User> getList();
}
