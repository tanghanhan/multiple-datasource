package cn.th.demo;

import cn.th.demo.dao.master.User1Mapper;
import cn.th.demo.dao.slave.User2Mapper;
import cn.th.demo.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class MultipleDatasourceDemoApplicationTests {
	@Resource
	private User1Mapper user1Mapper;
	@Resource
	private User2Mapper user2Mapper;

	@Test
	void test_list() {
		List<User> user1List = user1Mapper.getList();
		assertNotNull(user1List);
		List<User> user2List = user2Mapper.getList();
		assertNotNull(user2List);
		System.out.println("user1: ");
		user1List.forEach(System.out::println);
		System.out.println();
		System.out.println("user2: ");
		user2List.forEach(System.out::println);
	}

}
